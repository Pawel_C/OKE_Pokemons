package com.oke.pokenon;

import android.app.Application;

public class ApplicationTest extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        // do something important for your tests here
    }
}