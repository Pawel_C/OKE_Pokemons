package com.oke.pokenon.activities;

import android.test.ActivityInstrumentationTestCase2;

import android.widget.EditText;

import com.oke.pokenon.R;


public class AddNoteActivityTest extends ActivityInstrumentationTestCase2<AddNoteActivity> {

    private EditText nickname;

    private EditText notes;

    private AddNoteActivity testActivity;

    public AddNoteActivityTest() {
        super(AddNoteActivity.class);
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();

        testActivity = getActivity();
        nickname = (EditText) testActivity.findViewById(R.id.nickname);
        notes = (EditText) testActivity.findViewById(R.id.notes);
    }

    public void testNickname() {
        assertNotNull("nickname is null", nickname);
    }

    public void testActivity(){
        assertNotNull("Activity is null", testActivity);
    }

    public void testNotes(){
        assertNotNull("notes is null", notes);
    }

}
