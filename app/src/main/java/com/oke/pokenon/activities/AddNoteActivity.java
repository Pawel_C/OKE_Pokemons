package com.oke.pokenon.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.oke.pokenon.R;
import com.oke.pokenon.data.MyPokemon;
import com.oke.pokenon.db.PokemonDao;
import com.oke.pokenon.fragments.PokemonsDetailFragment;

public class AddNoteActivity extends AppCompatActivity implements View.OnClickListener {

    private int pokemonId;

    private EditText nickname;

    private EditText notes;

    private PokemonDao dao;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_note);

        Bundle bundle = getIntent().getExtras();
        pokemonId = bundle.getInt(PokemonsDetailFragment.ARG_ITEM_ID);

        dao = new PokemonDao(this);

        setActionBar();

        nickname = (EditText) findViewById(R.id.nickname);
        notes = (EditText) findViewById(R.id.notes);

        setEditTextValues();

        Button addNotes = (Button) findViewById(R.id.add_notes);
        addNotes.setOnClickListener(this);
    }

    @Override
    public void onClick(View view){
        showAddDialog();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            Intent intent = new Intent(this, PokemonDetailActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            navigateUpTo(intent);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void setActionBar(){
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    public void showAddDialog(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(getString(R.string.message_add_notes))
                .setNegativeButton(getString(R.string.button_no), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();

                    }
                })
                .setPositiveButton(getString(R.string.button_yes), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        updatePokemon();
                        dialog.cancel();
                        AddNoteActivity.this.finish();

                    }

                });
        AlertDialog alert = builder.create();
        alert.show();
    }

    private void updatePokemon(){
        PokemonDao dao = new PokemonDao(AddNoteActivity.this);

        MyPokemon pokemon = dao.get(pokemonId);
        pokemon.setNickname(nickname.getText().toString());
        pokemon.setNotes(notes.getText().toString());
        dao.update(pokemon);
    }

    public void setEditTextValues(){
        String nicknameString = dao.get(pokemonId).getNickname();
        String notesString = dao.get(pokemonId).getNotes();

        if(nicknameString != null) {
            nickname.setText(nicknameString);
        }

        if(notesString != null) {
            notes.setText(notesString);
        }
    }
}
