package com.oke.pokenon.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.ActionBar;
import android.view.MenuItem;

import com.oke.pokenon.R;
import com.oke.pokenon.fragments.PokemonsDetailFragment;

public class PokemonDetailActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pokemon_detail);
        setActionBar();

        int id = getIntent().getIntExtra(PokemonsDetailFragment.ARG_ITEM_ID, 0);

        if (savedInstanceState == null) {
            createDetailFragment(id);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            Intent intent = new Intent(this, PokemonListActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void setActionBar(){
        Toolbar toolbar = (Toolbar) findViewById(R.id.detail_toolbar);
        setSupportActionBar(toolbar);

        // Show the Up button in the action bar.
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    private void createDetailFragment(int id){
        Bundle arguments = new Bundle();
        arguments.putInt(PokemonsDetailFragment.ARG_ITEM_ID, id);

        PokemonsDetailFragment fragment = new PokemonsDetailFragment();
        fragment.setArguments(arguments);
        getSupportFragmentManager().beginTransaction()
                .add(R.id.items_detail_container_activity, fragment)
                .commit();
    }
}
