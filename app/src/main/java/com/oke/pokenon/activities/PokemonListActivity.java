package com.oke.pokenon.activities;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.support.v7.widget.Toolbar;

import com.oke.pokenon.R;

import com.oke.pokenon.adapters.PokemonListAdapter;
import com.oke.pokenon.data.MyPokemon;
import com.oke.pokenon.db.PokemonDao;
import com.oke.pokenon.dialogs.LoadingDialog;
import com.oke.pokenon.fragments.PokemonsDetailFragment;

import java.util.ArrayList;
import java.util.List;

import me.sargunvohra.lib.pokekotlin.client.PokeApi;
import me.sargunvohra.lib.pokekotlin.client.PokeApiClient;
import me.sargunvohra.lib.pokekotlin.model.NamedApiResource;
import me.sargunvohra.lib.pokekotlin.model.NamedApiResourceList;

public class PokemonListActivity extends AppCompatActivity {

    /**
     * Whether or not the activity is in two-pane mode, i.e. running on a tablet
     * device.
     */
    private boolean twoPane;

    private int rowItems;

    private RecyclerView recyclerView;

    private  ArrayList<MyPokemon> pokemons;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pokemon_list);
        setToolbar();

        if (findViewById(R.id.items_detail_container) != null) {
            twoPane = true;
            createDetailFragment(1);
        }
        rowItems = setRowItemsCount(twoPane);
        initRecyclerView();

        ConnectivityManager connMgr = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();

        getPokemonsFromDatabase();

        if(pokemons.size() == 0) {

            if (networkInfo != null && networkInfo.isConnected()) {
                getPokemonsFromApi();
            } else {

            }
        }else{
            showPokemonList();
        }
    }

    public void createDetailFragment(int pokemonId) {
        Bundle arguments = new Bundle();
        arguments.putInt(PokemonsDetailFragment.ARG_ITEM_ID, pokemonId);

        PokemonsDetailFragment fragment = new PokemonsDetailFragment();
        fragment.setArguments(arguments);

        getSupportFragmentManager().beginTransaction()
                .replace(R.id.items_detail_container, fragment)
                .commit();
    }

    private void setToolbar(){
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitle(getTitle());
    }

    private void showPokemonList(){
        PokemonListAdapter adapter = new PokemonListAdapter(PokemonListActivity.this, pokemons, twoPane);
        recyclerView.setAdapter(adapter);
    }

    public void getPokemonsFromApi(){
        RetrievePokemonsTask task = new RetrievePokemonsTask(0, 20);
        task.execute();
    }

    private void getPokemonsFromDatabase(){
        PokemonDao dao = new PokemonDao(this);
        pokemons = dao.getAll();

    }

    private void initRecyclerView(){
        recyclerView = (RecyclerView) findViewById(R.id.list);
        StaggeredGridLayoutManager staggeredGridLayoutManager = new StaggeredGridLayoutManager(rowItems,
                    StaggeredGridLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(staggeredGridLayoutManager);
    }

    public static int setRowItemsCount(boolean twoPane){
        if (twoPane) {
            return 1;
        }else{
            return 2;
        }
    }


    public class RetrievePokemonsTask extends AsyncTask<String, Void, List<NamedApiResource>> {

        private int first;

        private int last;

        private LoadingDialog progressDialog;

        public RetrievePokemonsTask(int first, int last){
            this.first = first;
            this.last = last;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new LoadingDialog(PokemonListActivity.this);
            progressDialog.setAnimation(R.drawable.anim_dialog);
            progressDialog.show();
        }

        protected List<NamedApiResource> doInBackground(String... urls) {

            try {
                PokeApi pokeApi = new PokeApiClient();
                NamedApiResourceList list = pokeApi.getPokemonList(first, last);
                return list.getResults();

            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }

        @Override
        protected void onPostExecute(List<NamedApiResource> feed) {
            pokemons = getPokemonList(feed);

            PokemonDao dao = new PokemonDao(getBaseContext());
            dao.addPokemons(pokemons);

            showPokemonList();
            progressDialog.cancel();
        }

        public ArrayList<MyPokemon> getPokemonList(List<NamedApiResource> feed){
            ArrayList<MyPokemon> list = new ArrayList<>();
            for(NamedApiResource resource: feed){
                list.add(new MyPokemon(resource));
            }
            return list;
        }
    }
}
