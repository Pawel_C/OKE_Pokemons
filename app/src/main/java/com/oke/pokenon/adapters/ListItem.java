package com.oke.pokenon.adapters;

public class ListItem{

    public String header;

    public String value;

    public int typeId;

    public ListItem(String header, String value){
        this.header = header;
        this.value = value;
    }
}