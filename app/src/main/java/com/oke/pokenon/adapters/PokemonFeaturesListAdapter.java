package com.oke.pokenon.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.oke.pokenon.R;

import java.util.ArrayList;

public class PokemonFeaturesListAdapter extends BaseAdapter{

    public Context context;

    private LayoutInflater inflater;

    private ArrayList<ListItem> listItems;

    public PokemonFeaturesListAdapter(Context context, ArrayList<ListItem> listItems) {
        this.context = context;
        this.inflater = LayoutInflater.from(context);
        this.listItems = listItems;
    }

    public int getCount() {
        return listItems.size();
    }

    public Object getItem(int position) {
        return listItems.get(position);
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {

        ListItem item = listItems.get(position);
        ViewHolder holder;

        if (convertView == null) {

            holder = new ViewHolder();
            if(item.header.equals("Types:") || item.header.equals("Features:"))
                convertView = inflater.inflate(R.layout.item_header, null);
            else
                convertView = inflater.inflate(R.layout.item_feature, null);

            holder.header = (TextView) convertView.findViewById(R.id.header);
            holder.value = (TextView) convertView.findViewById(R.id.value);
            convertView.setTag(holder);

        }else{
            holder = (ViewHolder) convertView.getTag();
        }
        holder.header.setText(item.header);
        holder.value.setText(item.value);
        return convertView;
    }

    static class ViewHolder {
        TextView value;
        TextView header;
    }
}