package com.oke.pokenon.adapters;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.oke.pokenon.R;
import com.oke.pokenon.activities.PokemonDetailActivity;
import com.oke.pokenon.data.MyPokemon;
import com.oke.pokenon.fragments.PokemonsDetailFragment;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Locale;


public class PokemonListAdapter
        extends RecyclerView.Adapter<PokemonListAdapter.ViewHolder> {

    private final ArrayList<MyPokemon> pokemons;

    private boolean twoPane;

    private Context context;

    public PokemonListAdapter(Context context, ArrayList<MyPokemon> pokemons, boolean twoPane) {
        this.pokemons = pokemons;
        this.context = context;
        this.twoPane = twoPane;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_pokemon, parent, false);
        return new ViewHolder(view);
    }

    public void openPokemonDetailActivity(int id) {
        Intent intent = new Intent(context, PokemonDetailActivity.class);
        intent.putExtra(PokemonsDetailFragment.ARG_ITEM_ID, id);
        context.startActivity(intent);
    }

    public void createDetailFragment(int pokemonId) {
        Bundle arguments = new Bundle();
        arguments.putInt(PokemonsDetailFragment.ARG_ITEM_ID, pokemonId);

        PokemonsDetailFragment fragment = new PokemonsDetailFragment();
        fragment.setArguments(arguments);

        AppCompatActivity activity = (AppCompatActivity) context;
        activity.getSupportFragmentManager().beginTransaction()
                .replace(R.id.items_detail_container, fragment)
                .commit();
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.pokemon = pokemons.get(position);
        holder.name.setText(pokemons.get(position).getName());
        String imageUrl = "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/back/%d.png";

        Picasso.with(context)
                .load(String.format(Locale.ENGLISH,imageUrl, holder.pokemon.getId()))
                .into(holder.image);

        holder.mView.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (twoPane) {
                    createDetailFragment(holder.pokemon.getId());
                } else {
                    openPokemonDetailActivity(holder.pokemon.getId());
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return pokemons.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public final View mView;

        private ImageView image;

        public final TextView name;

        public MyPokemon pokemon;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            name = (TextView) itemView.findViewById(R.id.pokemon_name);
            image = (ImageView) itemView.findViewById(R.id.pokemon_photo);
        }
    }
}