package com.oke.pokenon.data;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;
import com.oke.pokenon.adapters.ListItem;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Locale;

import me.sargunvohra.lib.pokekotlin.model.NamedApiResource;
import me.sargunvohra.lib.pokekotlin.model.Pokemon;
import me.sargunvohra.lib.pokekotlin.model.PokemonSprites;
import me.sargunvohra.lib.pokekotlin.model.PokemonType;

@DatabaseTable(tableName = "pokemon")
public class MyPokemon implements Serializable {

    @DatabaseField(id = true, columnName = "id")
    private int id;

    @DatabaseField(columnName = "name")
    private String name;

    @DatabaseField(columnName = "weight")
    private int weight;

    @DatabaseField(columnName = "height")
    private int height;

    @DatabaseField(columnName = "front")
    private String front;

    @DatabaseField(columnName = "back")
    private String back;

    @DatabaseField(columnName = "base_experience")
    private int baseExperience;

    @DatabaseField(columnName = "notes")
    private String notes;

    @DatabaseField(columnName = "nickname")
    private String nickname;

    @ForeignCollectionField(eager = true, foreignFieldName = "pokemon",maxEagerLevel = 1)
    private Collection<MyPokemonType> types;

    public MyPokemon(){

    }

    public MyPokemon(Pokemon pokemon){
        PokemonSprites sprites = pokemon.getSprites();
        setName(pokemon.getName());
        setHeight(pokemon.getHeight());
        setWeight(pokemon.getWeight());
        setBaseExperience(pokemon.getBaseExperience());
        setFront(sprites.getFrontDefault());
        setBack(sprites.getBackDefault());
        setTypes(pokemon.getTypes());
    }

    public MyPokemon(NamedApiResource resource){
        setName(resource.getName());
        setId(resource.getId());
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getFront() {
        return front;
    }

    public void setFront(String front) {
        this.front = front;
    }

    public String getBack() {
        return back;
    }

    public void setBack(String back) {
        this.back = back;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public ArrayList<MyPokemonType> getTypes() {
        if(types != null) {
            return new ArrayList<>(types);
        }else{
            return null;
        }
    }

    public ArrayList<ListItem> getTypesAsListItems() {
        ArrayList<ListItem> list = new ArrayList<>();
        ArrayList<MyPokemonType> typesArray = getTypes();

        for(int i = 0; i < typesArray.size(); i++){
            MyPokemonType type = typesArray.get(i);
            ListItem item = new ListItem(String.format(Locale.ENGLISH,"Type %d", i), type.getName() );
            item.typeId = type.getId();
            list.add(item);
        }
        return list;
    }

    public void setTypes(List<PokemonType> types) {
        this.types = new ArrayList<>();

        for(PokemonType type: types) {
            NamedApiResource namedApiResource = type.getType();
            MyPokemonType newType = new MyPokemonType();
            newType.setId(namedApiResource.getId());
            newType.setName(namedApiResource.getName());
            this.types.add(newType);
        }
    }

    public int getBaseExperience() {
        return baseExperience;
    }

    public void setBaseExperience(int base_experience) {
        this.baseExperience = base_experience;
    }

}
