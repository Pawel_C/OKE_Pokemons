package com.oke.pokenon.data;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

public class MyPokemonHasType {

    @DatabaseTable(tableName = "pokemonHasType")
    public class Test1Test2Join {

        @DatabaseField(generatedId = true)
        private long id;

        @DatabaseField(foreign = true)
        private MyPokemon pokemon;

        @DatabaseField(foreign = true)
        private MyPokemonType type;

    }
}
