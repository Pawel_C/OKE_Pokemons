package com.oke.pokenon.data;

import com.j256.ormlite.field.DatabaseField;

import java.io.Serializable;

public class MyPokemonType implements Serializable {

    @DatabaseField(columnName = "id_", generatedId = true)
    private int id;

    @DatabaseField(columnName = "name")
    private String name;

    @DatabaseField(foreign = true,  foreignAutoCreate=true, foreignAutoRefresh=true)
    public MyPokemon pokemon;

    public MyPokemonType(){

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public MyPokemon getPokemon() {
        return pokemon;
    }

    public void setPokemon(MyPokemon pokemon) {
        this.pokemon = pokemon;
    }
}
