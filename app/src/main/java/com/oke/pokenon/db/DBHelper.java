package com.oke.pokenon.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;
import com.oke.pokenon.data.MyPokemon;
import com.oke.pokenon.data.MyPokemonType;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

public class DBHelper extends OrmLiteSqliteOpenHelper {

    public static final String DB_NAME = "pokemon.db";

    public static final int DB_VERSION = 1;

    public Context context;

    public DBHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
        this.context = context;
        getWritableDatabase();
    }

    @Override
    public void onCreate(SQLiteDatabase db, ConnectionSource cs) {
        try {
            TableUtils.createTable(cs, MyPokemon.class);
            TableUtils.createTable(cs, MyPokemonType.class);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, ConnectionSource cs, int oldVersion, int newVersion) {
    }

    public <T> T get(Class<T> className, long id){
        T object = null;
        try {
            Dao<T, Long> dao = getDao(className);
            object = dao.queryForId(id);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return object;
    }

    public <T> List<T> get(Class<T> className, String fieldName, Object value){
        List<T> object = null;
        try {
            Dao<T, String> dao = getDao(className);
            object = dao.queryForEq(fieldName, value);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return object;
    }

    public <T> List<T> get(Class<T> className, Map<String, Object> map){
        List<T> object = null;
        try {
            Dao<T, String> dao = getDao(className);
            object = dao.queryForFieldValues(map);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return object;
    }


    public <T> void assignEmptyForeignCollection(Class<T> className, T obj, String tableName){
        try {
            Dao<T, ?> dao = getDao(className);
            dao.assignEmptyForeignCollection(obj, tableName);

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public <T> int update(Class<T> className, T obj){
        int status = 0;
        try {
            Dao<T, ?> dao = getDao(className);
            status = dao.update(obj);

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return status;
    }

    public <T> int create(Class<T> className, T obj){
        int status = 0;
        try {
            Dao<T, ?> dao = getDao(className);
            status =dao.create(obj);

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return status;
    }

    public <T> List<T> getAll(Class<T> className){
        List<T> list = null;
        try {
            Dao<T, ?> dao = getDao(className);
            list = dao.queryForAll();
            for(T obj: list) {
                dao.refresh(obj);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return list;
    }
}