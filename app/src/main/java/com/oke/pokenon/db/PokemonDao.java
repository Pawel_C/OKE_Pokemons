package com.oke.pokenon.db;

import android.content.Context;

import com.oke.pokenon.data.MyPokemon;
import com.oke.pokenon.data.MyPokemonType;

import java.util.ArrayList;

public class PokemonDao {

    private DBHelper helper;

    private PokemonTypeDao typeDao;

    public PokemonDao(Context context){
        helper  = new DBHelper(context);
        typeDao = new PokemonTypeDao(context);
    }

    public MyPokemon get(long id){

        return helper.get(MyPokemon.class, id);
    }

    public ArrayList<MyPokemon> getAll(){
        return  new ArrayList<>(helper.getAll(MyPokemon.class));
    }

    public void add(MyPokemon pokemon){
        helper.create(MyPokemon.class, pokemon);
    }

    public int update(MyPokemon pokemon){
        if(pokemon.getTypes() != null) {
            for(MyPokemonType type: pokemon.getTypes()) {
                type.setPokemon(pokemon);
                typeDao.add(type);
            }
        }
        return helper.update(MyPokemon.class, pokemon);
    }

    public void addPokemons(ArrayList<MyPokemon> pokemons){
        for(MyPokemon pokemon: pokemons){
            add(pokemon);
        }
    }
}
