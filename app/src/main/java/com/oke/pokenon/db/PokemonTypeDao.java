package com.oke.pokenon.db;

import android.content.Context;

import com.oke.pokenon.data.MyPokemonType;

import java.util.List;

public class PokemonTypeDao {

    private DBHelper helper;

    public PokemonTypeDao(Context context){
        helper  = new DBHelper(context);
    }

    public MyPokemonType get(long id){
        return helper.get(MyPokemonType.class, id);
    }

    public List<MyPokemonType> getAll(){
        return  helper.getAll(MyPokemonType.class);
    }

    public void add(MyPokemonType type){
        helper.create(MyPokemonType.class, type);
    }

    public void update(MyPokemonType type){
        helper.update(MyPokemonType.class, type);
    }
}