package com.oke.pokenon.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.AnimationDrawable;
import android.view.Window;
import android.widget.ImageView;

import com.oke.pokenon.R;

public class LoadingDialog extends Dialog{

    public LoadingDialog(Context context){
        super(context);
        setCancelable(false);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_loading);
    }

    public void setAnimation(int anim_id){
        ImageView image = (ImageView) findViewById(R.id.dialog_loading_image);
        image.setBackgroundResource(anim_id);

        AnimationDrawable rocketAnimation = (AnimationDrawable) image.getBackground();
        rocketAnimation.start();
    }

}
