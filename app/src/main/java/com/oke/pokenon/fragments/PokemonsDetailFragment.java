package com.oke.pokenon.fragments;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;

import com.oke.pokenon.R;
import com.oke.pokenon.activities.AddNoteActivity;
import com.oke.pokenon.activities.PokemonDetailActivity;
import com.oke.pokenon.activities.PokemonListActivity;
import com.oke.pokenon.adapters.ListItem;
import com.oke.pokenon.adapters.PokemonFeaturesListAdapter;
import com.oke.pokenon.data.MyPokemon;
import com.oke.pokenon.data.MyPokemonType;
import com.oke.pokenon.db.PokemonDao;
import com.oke.pokenon.dialogs.LoadingDialog;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import me.sargunvohra.lib.pokekotlin.client.PokeApi;
import me.sargunvohra.lib.pokekotlin.client.PokeApiClient;
import me.sargunvohra.lib.pokekotlin.model.Pokemon;
import me.sargunvohra.lib.pokekotlin.model.Type;

/**
 * A fragment representing a single Items detail screen.
 * This fragment is either contained in a {@link PokemonListActivity}
 * in two-pane mode (on tablets) or a {@link PokemonDetailActivity}
 * on handsets.
 */
public class PokemonsDetailFragment extends Fragment implements View.OnClickListener, AdapterView.OnItemClickListener{

    public static final String ARG_ITEM_ID = "item_id";

    private PokemonDao dao;

    private ImageView frontView;

    private ImageView backView;

    private ListView featuresListView;

    private MyPokemon pokemon;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public PokemonsDetailFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_pokemons_detail, container, false);
        frontView = (ImageView) rootView.findViewById(R.id.pokemonFront);
        backView = (ImageView) rootView.findViewById(R.id.pokemonBack);

        featuresListView = (ListView) rootView.findViewById(R.id.features_list);
        featuresListView.setOnItemClickListener(this);

        FloatingActionButton addNotes = (FloatingActionButton) rootView.findViewById(R.id.add_notes);
        addNotes.setOnClickListener(this);

        ConnectivityManager connMgr = (ConnectivityManager)getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();

        dao = new PokemonDao(getContext());

        if (getArguments().containsKey(ARG_ITEM_ID)) {

            int id = getArguments().getInt(PokemonsDetailFragment.ARG_ITEM_ID, 0);
            pokemon = getPokemonFromDatabase(id);

            if(pokemon == null || pokemon.getHeight() == 0) {
                if (networkInfo != null && networkInfo.isConnected()) {
                    getPokemonFromApi(id);
                }
            }else {
                refreshView();
            }
        }
        return rootView;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id){

        if(position > 5){
            MyPokemonType type = pokemon.getTypes().get(position-6);
            showInfoDialog(type.getName());
//            getTypeFromApi(pokemon.getTypes().get(position-6).getId());
        }

    }

    @Override
    public void onClick(View view){
        switch (view.getId()){
            case R.id.add_notes:
                Intent intent = new Intent(this.getActivity(), AddNoteActivity.class);
                Bundle bundle = new Bundle();
                bundle.putInt(ARG_ITEM_ID, pokemon.getId());
                intent.putExtras(bundle);
                startActivity(intent);
                break;
        }
    }

    public void showImages(){

        Picasso.with(this.getContext())
                .load(pokemon.getFront())
                .into(frontView);

        Picasso.with(this.getContext())
                .load(pokemon.getBack())
                .into(backView);
    }

    private void showFeatures(){

        ArrayList<ListItem> features = new ArrayList<>();
        features.add(new ListItem("Features:", ""));
        features.add(new ListItem("Name:", pokemon.getName()));
        features.add(new ListItem("Height:",String.valueOf(pokemon.getHeight())));
        features.add(new ListItem("Weight:",String.valueOf(pokemon.getHeight())));
        features.add(new ListItem("Base experience:",String.valueOf(pokemon.getBaseExperience())));
        features.add(new ListItem("Types:",""));
        features.addAll(pokemon.getTypesAsListItems());

        PokemonFeaturesListAdapter adapter = new PokemonFeaturesListAdapter(this.getContext(), features);
        featuresListView.setAdapter(adapter);
    }

    private MyPokemon getPokemonFromDatabase(int id){
        return dao.get(id);
    }

    private void getPokemonFromApi(int id){
        RetrievePokemonTask pokemonTask = new RetrievePokemonTask(id);
        pokemonTask.execute();
    }

    private void getTypeFromApi(int id){
        RetrievePokemonType type = new RetrievePokemonType(id);
        type.execute();
    }

    private void updatePokemon(){
        dao.update(pokemon);
    }

    private void refreshView(){
        showImages();
        showFeatures();
    }

    public void showInfoDialog(String message){
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setMessage(message)
                .setNegativeButton(getString(R.string.button_ok), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }

    class RetrievePokemonTask extends AsyncTask<String, Void, Pokemon> {

        private int id;

        private LoadingDialog progressDialog;

        public RetrievePokemonTask(int id) {
            this.id = id;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new LoadingDialog(getContext());
            progressDialog.setAnimation(R.drawable.anim_dialog);
            progressDialog.show();
        }

        protected Pokemon doInBackground(String... urls) {
            try {
                PokeApi pokeApi = new PokeApiClient();
                return pokeApi.getPokemon(id);
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }

        protected void onPostExecute(Pokemon pokemon) {
            PokemonsDetailFragment.this.pokemon = new MyPokemon(pokemon);
            // server object doesn't contain id
            PokemonsDetailFragment.this.pokemon.setId(id);
            updatePokemon();
            refreshView();
            progressDialog.cancel();
        }
    }


    class RetrievePokemonType extends AsyncTask<String, Void, Type> {

        private int id;

        private LoadingDialog progressDialog;

        public RetrievePokemonType(int id) {
            this.id = id;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new LoadingDialog(getContext());
            progressDialog.setAnimation(R.drawable.anim_dialog);
            progressDialog.show();
        }

        protected Type doInBackground(String... urls) {
            try {
                PokeApi pokeApi = new PokeApiClient();
                return pokeApi.getType(id);
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }

        protected void onPostExecute(Type type) {
            showInfoDialog(type.toString());
            progressDialog.cancel();
        }
    }
}
