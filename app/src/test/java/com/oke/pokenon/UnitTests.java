package com.oke.pokenon;

import com.oke.pokenon.activities.PokemonListActivity;
import com.oke.pokenon.fragments.PokemonsDetailFragment;

import org.junit.Test;


import static org.junit.Assert.*;

/**
 * To work on unit tests, switch the Test Artifact in the Build Variants view.
 */

public class UnitTests {

    @Test
    public void verifyOneItemRow() throws Exception {
        int actual = PokemonListActivity.setRowItemsCount(true);
        int expected = 1;
        assertEquals("Count row items failed", expected, actual);
    }

    @Test
    public void verifyTwoItemRows() throws Exception {
        int actual = PokemonListActivity.setRowItemsCount(false);
        int expected = 2;
        assertEquals("Count row items failed", expected, actual);
    }

    @Test
    public void nullStringTest() {
        String id = PokemonsDetailFragment.ARG_ITEM_ID;
        assertNotNull(id);
    }

}